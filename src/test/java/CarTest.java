import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class CarTest {

    @Mock
    Car newCar;

    @BeforeEach
    void setup(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testMethodGetManufacture(){
        Car car = new Car("ferrari","123fbc",2006, "Stev Kovalski");
        assertEquals("ferrari", car.getManufacture());
    }

    @Test
    void testMethodGetManufactureOnMockito(){
        assertEquals(null, newCar.getManufacture());
        assertEquals(0, newCar.getYear());
    }

    @Test
    void remonteServissReturnValue(){

        //Если вызываем testInt с аргументом 4 , то нужно вернуть 10
        when(newCar.testInt(4)).thenReturn(10);
        assertEquals(10, newCar.testInt(4));
    }

    @Test
    void testGetOwner(){

        //Возвращает по умолчанию Дмитрия всегда
        when(newCar.getOwner()).thenReturn("Dmitryi");
        assertEquals("Dmitryi", newCar.getOwner());
    }

    @Test
    void testVerefication(){

        //Проверяет вызывался метод или нет ранее и неважно в каком контексте, можно даже в методе assertEquals()
        newCar.getOwner();
        assertEquals(null, newCar.getOwner());
        verify(newCar).getOwner();
    }

    @Test
    void testingTestInt(){

        //Проверяет вызывался метод с аргументом 4 или нет
        newCar.testInt(4);
        verify(newCar).testInt(4);

    }

    @Test
    void testVereficationCall(){

        assertEquals(null, newCar.getOwner());
        verify(newCar, times(1)).getOwner();

        verify(newCar, atMost(1)).getOwner();

        //Прверяет что вызывался только getOwner(). Усли вызывалось что-то еще, то будет ошибка
        verify(newCar, only()).getOwner();


    }
}