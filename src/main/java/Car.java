import java.util.ArrayList;
import java.util.List;

public class Car {

    private String manufacture;
    private String number;
    private int year;
    private String owner;

    private List<String> owners = new ArrayList();


    public Car(String manufacture, String number, int year, String owner) {
        this.manufacture = manufacture;
        this.number = number;
        this.year = year;
        this.owner = owner;
        owners.add(owner);
    }

    public String getManufacture() {
        return manufacture;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getYear() {
        return year;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
        owners.add(owner);
    }

    public List<String> getOwners() {
        return owners;
    }

    private String testMethod(){
        return "abc";
    }

    private String testMethod(String a){
        return a;
    }

    public int testInt(int a){
        return a+4;
    }

    public void getDataFromRemoteServer() throws Exception {
        throw new Exception("error !!!");
    }


}
